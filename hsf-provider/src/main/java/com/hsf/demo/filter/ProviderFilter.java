package com.hsf.demo.filter;


import com.taobao.hsf.invocation.Invocation;
import com.taobao.hsf.invocation.InvocationHandler;
import com.taobao.hsf.invocation.RPCResult;
import com.taobao.hsf.invocation.filter.ServerFilter;
import com.taobao.hsf.util.PojoUtils;
import com.taobao.hsf.util.concurrent.ListenableFuture;


/**
 * @author sen.wang@hand-china.com
 * @description
 */
public class ProviderFilter implements ServerFilter {

    @Override
    public ListenableFuture<RPCResult> invoke(InvocationHandler invocationHandler, com.taobao.hsf.invocation.Invocation invocation) throws Throwable {
        // 获取方法参数类型
        String[]  sigs = invocation.getMethodArgSigs();
        // 获取方法的值
        Object [] args = invocation.getMethodArgs();
        System.out.print("获取的参数类型是： ");
        for(String sig : sigs) {
            System.out.print(sig);
            System.out.print(";");
        }
        System.out.println();
        System.out.print("传入的参数值是： ");
        for(Object arg : args) {
            System.out.print(PojoUtils.generalize(arg));
            System.out.print(";");
        }
        System.out.println();
        return invocationHandler.invoke(invocation);
    }

    @Override
    public void onResponse(Invocation invocation, RPCResult rpcResult) {
        Object resp = rpcResult.getHsfResponse().getAppResponse();
        System.out.println("返回的数据是：  "+PojoUtils.generalize(resp));
    }
}
