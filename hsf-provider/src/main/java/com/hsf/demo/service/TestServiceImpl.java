package com.hsf.demo.service;

import com.alibaba.dubbo.rpc.RpcContext;

import com.hsf.demo.TestService;

import java.util.Map;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/30 13:18
 */
public class TestServiceImpl implements TestService {
    public String getStr(String str) {
        return str;
    }

    /**
     * 隐式传参
     *
     * @param str
     * @return
     */
    public String getStr2(String str) {
        Map<String, String> map = RpcContext.getContext().getAttachments();
        System.out.println("获取map数据---------------");
        for (String att : map.keySet()) {
            System.out.println(att + "---" + map.get(att));
        }
        System.out.println("获取单个数据---------------");
        String name = RpcContext.getContext().getAttachment("name");
        System.out.println("name--" + name);
        return str;
    }

    /**
     * callback异步调用
     * @param str
     * @return
     */
    public String getStr3(String str) {
        try {
            Thread.sleep(1000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return str + "--callback异步调用1";
    }

    /**
     * future异步调用方法1
     * @param str
     * @return
     */
    public String getStr4(String str) {
        try {
            Thread.sleep(1000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return str+"--future异步调用1";
    }
    /**
     * future异步调用方法2
     * @param str
     * @return
     */
    public String getStr5(String str) {
        try {
            Thread.sleep(2000l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return str+"--future异步调用2";
    }
}
