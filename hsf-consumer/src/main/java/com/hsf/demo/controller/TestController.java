package com.hsf.demo.controller;

import com.alibaba.dubbo.rpc.RpcContext;

import com.taobao.hsf.exception.HSFException;
import com.taobao.hsf.tbremoting.invoke.HSFFuture;
import com.taobao.hsf.tbremoting.invoke.HSFResponseFuture;

import com.hsf.demo.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/30 15:16
 */
@Controller
public class TestController {
    @Autowired
    @Qualifier(value = "item")
    private TestService service;

    @RequestMapping(value = "/getStr",method = RequestMethod.GET)
    public void getStr(String str, HttpServletResponse response) throws IOException {
        response.getWriter().write(service.getStr(str));
    }

    /**
     * 隐式传参
     * @param str
     */
    @RequestMapping(value = "/getStr2",method = RequestMethod.GET)
    public void getStr2(String str, HttpServletResponse res) throws IOException {
        // 传多个参数
        Map<String, String> map = new HashMap(2);
        map.put("age", "110");
        map.put("gender", "male");
        RpcContext.getContext().setAttachments(map);

        // 传单个参数
        RpcContext.getContext().setAttachment("name", "sen");
        String re = service.getStr2(str);
        res.getWriter().write(re);
    }

    /**
     * callback异步调用
     */
    @RequestMapping(value = "/getStr3",method = RequestMethod.GET)
    public void getStr3(String str, HttpServletResponse res) throws IOException {
        // 不会获得返回值
        String re = service.getStr3(str);
        res.getWriter().write(str);
    }

    /**
     * future异步调用方法
     */
    @RequestMapping(value = "/getStr4",method = RequestMethod.GET)
    public void getStr4(String str, HttpServletResponse res) throws IOException {
        List<HSFFuture> futureList = new ArrayList<HSFFuture>();
        HSFFuture future = null;
        try {
            Long t1 = System.currentTimeMillis();
            // 分别异步调用str4和str5
            service.getStr4(str);
            future = HSFResponseFuture.getFuture();
            futureList.add(future);

            service.getStr5(str);
            future = HSFResponseFuture.getFuture();
            futureList.add(future);

            for (HSFFuture hsfFuture : futureList) {
                // 获取返回的结果
                String msg=(String) hsfFuture.getResponse(3000L);
                System.out.println(msg);
            }
            Long useTime = System.currentTimeMillis() - t1;
            System.out.println("总共用时---"+useTime);
        } catch (HSFException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        res.setContentType("text/html; charset=UTF-8");
        res.getWriter().write("over");
    }



}
