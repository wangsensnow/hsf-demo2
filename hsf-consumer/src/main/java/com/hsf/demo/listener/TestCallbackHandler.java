package com.hsf.demo.listener;

import com.taobao.hsf.exception.HSFException;
import com.taobao.hsf.tbremoting.invoke.HSFResponseCallback;

/**
 * @author sen.wang@hand-china.com
 * @description future异步调用的回调接口
 * @date 2018/6/9 15:19
 */

public class TestCallbackHandler implements HSFResponseCallback {
    @Override
    public void onAppException(Throwable throwable) {

    }

    @Override
    public void onAppResponse(Object o) {
        // 获取异步调用的结果
        System.out.println("listener监听返回结果");
        System.out.println((String) o);
    }

    @Override
    public void onHSFException(HSFException e) {

    }
}
