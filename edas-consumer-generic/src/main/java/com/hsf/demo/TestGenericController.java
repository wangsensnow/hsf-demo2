package com.hsf.demo;


import com.alibaba.dubbo.rpc.service.GenericService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author sen.wang@hand-china.com
 * @description
 * @date 2018/6/10 14:02
 */
@Controller
public class TestGenericController {

    @RequestMapping(value = "/getStr")
    public void getStrGeneric(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(req.getSession().getServletContext());
        // 强转成GenericService类，
        GenericService gs = (GenericService) applicationContext.getBean("item");
        // 调用方法。
        String result = (String) gs.$invoke("getStr", new String[]{"java.lang.String"}, new Object[]{"hello"});

        System.out.println(result);
        res.getWriter().write(result);
    }
}
